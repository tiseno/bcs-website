/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function dataValidate()
{
    var name = document.getElementById("txtName");
    var mobile = document.getElementById("txtMobile");
    var email = document.getElementById("txtEmail");
    var comments = document.getElementById("txaComments");
    var title;

    for (index=0; index < document.form1.title.length; index++) 
    {
        if (document.form1.title[index].checked) 
        {
            title = "1";
            break;
        }
        else
        {
            title = "";
        }
    }
	//var title = document.getElementsByName("title");
   
    if(name.value=="" && mobile.value=="" && email.value=="" && comments.value=="" && title =="")
    {
        alert("Please fill in all fields!");
    }
    else
    {
	if(title =="")
        {
            alert("Please choose a title!");
        }
        else if(name.value=="")
        {
            alert("Please fill in your name!");
        }
        else if(mobile.value=="")
        {
            alert("Please fill in your mobile number!");
        }
        else if(email.value=="")
        {
            alert("Please fill in your email address!");
        }
        else if(comments.value=="")
        {
            alert("Please fill in your comments!");
        }
        else
        {
            document.form1.action = "sendmail.php";
            document.form1.submit();
        }
    }
}

function valEmail()
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.getElementById("txtEmail").value;
    var status = new Boolean();
    if(reg.test(address) == false) 
    {
      alert('Invalid Email Address');
      document.getElementById("txtEmail").value="";
      document.getElementById("txtEmail").style.background="grey";
      status = false;
    }
    else
    {
        document.getElementById("txtEmail").style.background="white";
        status = true;
    }
    return status;
}

function valMobile()
{
    var phone = document.getElementById("txtMobile").value;
    var phoneRegex = /^\d\d\d\d\d\d\d\d\d\d$/;
    var status = new Boolean();
    if(!phone.match( phoneRegex )) 
    {
        alert("Invalid phone number!");
        document.getElementById("txtMobile").value="";
        document.getElementById("txtMobile").style.background="grey";
        status = false;
    }
    else
    {
        document.getElementById("txtMobile").style.background="white";
        status = true;
    }
    return status;
}

function valName()
{
    var name = document.getElementById("txtName").value;
    var nameReg = /^([A-Za-z\s])+$/;
    var status = new Boolean();
        if(nameReg.test(name.trim())==false)
        {
            alert("Invalid name, don't put any number or symbol!");
            document.getElementById("txtName").value="";
            document.getElementById("txtName").style.background="grey";
            status = false;
        }
        else
        {
            status = true;
            document.getElementById("txtName").style.background="white";
        }
    return status;
}

//function valTitle () 
//{ 
// var radios = document[frmName].elements[rbGroupName]; 
// for (var i=0; i <radios.length; i++) { 
//  if (radios[i].checked) { 
//   return true; 
//  } 
// } 
// return false; 
//} 

//function valTitle() { 
// if (!checkRadio("form1","title")) 
//    alert("You didnt select any title"); 
// else 
// 	status = true;
//    document.getElementByName("title"); 
//} 